const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./routes/taskRoute');

const port = 5000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb+srv://09CGarcia:patatasako23@cluster0.sjmpx.mongodb.net/s31?retryWrites=true&w=majority`, 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
).then(() => console.log(`Connected to Database!`))
.catch((err) => console.log(err));

app.use("/", taskRoute);

app.listen(port, () => console.log(`Server is running at port ${port}`))