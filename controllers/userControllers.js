const User = require('./../models/User');

//insert a new user
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

		return newUser.save().then( (savedUser, error) => {
		if(error){
			return error
		} else {
			return savedUser
		}
	});
}

// retrieve all users
module.exports.listOfUsers = () => {
	return User.find({}).then((err, result) =>{
		if (err){
			return err;
		}else {
			return result;
		}
	});
};

// retrieve specific user
module.exports.userById = (params) => {
	return User.findById(params).then((err, result) =>{
		if (err){
			return err;
		}else {
			return result;
		}
	});
};

// update user
module.exports.updateUser = (params, reqBody) => {
	return User.findByIdAndUpdate(params, reqBody, {new: true}).then((err, result)=>{
		if (err){
			return err;
		}else {
			return result;
		}
	});
};

// delete user
module.exports.deleteUser = (params) => {
	return User.findByIdAndDelete(params).then((err, result) =>{
		if (err){
			return err;
		}else {
			return result;
		}
	});
};